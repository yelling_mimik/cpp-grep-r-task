
# General info

This project is an attempt to write simplfied version of Linux 'grep -r' command in C++, using Boost library.

Grep, short for “global regular expression print”, is a command used for searching and matching text patterns in files contained in the regular expressions and comes pre-installed in every Linux distribution. Using '-r' flag results searching in current directory and all other subdirectories (recursively). 

## Technologies
* Ubuntu 22.04.3 LTS on Windows 10 x86_64 
* C++14
* g++ 11.4.0
* Boost
* Bash

## Getting started

You need to install these Boost libraries before 'making' program:

```
lboost_program_options 
lboost_filesystem 
lboost_thread 
```

Installing above Boost libraries on Ubuntu:
```
sudo apt-get install libboost-program-options-dev libboost-filesystem-dev libboost-thread-dev 
```

Than go to ./grep-r/src and run 'make', next commands will be executed:
```

mkdir obj 
g++ -std=c++14 -Wall -O3 -I../../include/ -c read_parameters.cpp -o obj/read_parameters.o
g++ -std=c++14 -Wall -O3 -I../../include/ -c search_files.cpp -o obj/search_files.o
g++ -std=c++14 -Wall -O3 -I../../include/ -c output_data.cpp -o obj/output_data.o
g++ -std=c++14 -Wall -O3 -I../../include/ -c main.cpp -o obj/main.o
g++ -std=c++14 -Wall -O3 -I../../include/ obj/main.o obj/read_parameters.o obj/search_files.o obj/output_data.o -lboost_program_options -lboost_filesystem -lboost_thread  -o main  

```

Run 'make clean' if you want to remove compiled files, and than make to compile again 

Now you can run ./main


### Mandatory positional parametr
string to search

### Additional parameters

```

Generic_options_desc options:
  -v [ --version ]                      prints program version
  -h [ --help ]                         produce help message
  -c [ --config ] arg (=./../config_files/multiple_sources.conf)
                                        name of a configuration file

Configuration:
  -d [ --dir ] arg (=.)                 start directory where program needs to 
                                        look for files (also in subfolders)
  -l [ --log_file ] arg (=greper.log)     name of log file 
  -r [ --result_file ] arg (=greper.txt)  name of result file
  -t [ --threads ] arg (=4)             number of threads in the pool

```

### Config file
If ran without parametrs output will be similar to this:

```

Config file:    ./../config_files/multiple_sources.conf
Input data:     your input data
Directory:      .
Log file:       ./greper.log
Result file:    ./greper.txt
Threads:        4
Searched files:     10
Files with pattern: 0
Patterns number:    0
Result file:        ./greper.txt
Log file:           ./greper.log
Used threads:       4
Elapsed time:       22 miliseconds

```

By default all parametrs are taken from default config file, if they aren't inputed by user. In case you want to run programm with same paramerts several times you can modify config file, which lies under /grep-r/config_files/multiple_sources.conf and run without entering parametrs into terminal, or create several config files with different parametrs and input only path to spesific config file, when run. 
