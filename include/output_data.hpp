#include <boost/filesystem.hpp>
#include <iostream>
#include <chrono>

namespace fs = boost::filesystem;

std::string get_line_by_number(const std::string&, const int&);

void print_output_to_cmd(   const uint&,
                            const uint&,
                            const uint&,
                            const std::string&,
                            const std::string&, 
                            const uint&, 
                            const uint&);

template<typename KeyType, typename ValueType>
void write_data_to_result_file(std::ofstream& file, std::vector<std::pair<KeyType, std::vector<ValueType>>>& sorted_results_map)
{
    for (const auto& pair : sorted_results_map) 
    {
        for (const auto& i : pair.second) 
        {
            file << pair.first << " : " <<  i << " : "  << get_line_by_number(pair.first, i) << "\n";
        }
        file << "\n";
    }
}

template<typename KeyType, typename ValueType>
void write_data_to_log_file(std::ofstream& file, std::vector<std::pair<KeyType, std::vector<ValueType>>>& sorted_logs_map)
{
    for (const auto& pair : sorted_logs_map) 
    {
        file << pair.first << " : ";
        for (const auto& i : pair.second) 
        {
            file << i << "; ";
        }
        file << "\n";
    }
}

template<typename KeyType, typename ValueType>
void write_program_data(std::string &file_name, 
                        std::map<const KeyType, std::vector<ValueType>>& output_map, 
                        void (*writing_func)(std::ofstream&, std::vector<std::pair<KeyType, std::vector<ValueType>>>&))
{
    // Sort the map by vector size
    std::vector<std::pair<KeyType, std::vector<ValueType>>> sorted_vector_of_pairs(output_map.begin(), output_map.end());
    std::sort(sorted_vector_of_pairs.begin(), sorted_vector_of_pairs.end(), compare_vectors<std::pair<KeyType, std::vector<ValueType>>>);

    std::ofstream file(file_name);
    if (!file.is_open()) 
    {
        std::cerr << "Error: could not open file." << "\n";
    }

    writing_func(file, sorted_vector_of_pairs);
    file.close();
}

uint get_elapsed_time(  const std::chrono::_V2::system_clock::time_point, 
                        const std::chrono::_V2::system_clock::time_point);
