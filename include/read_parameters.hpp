#include "boost/program_options.hpp"
#include "boost/filesystem/operations.hpp" // includes boost/filesystem/path.hpp
#include <iostream>
#include <fstream>
#include <algorithm>    // std::copy
#include <iterator>
#include <vector>       

// A helper function to simplify the main part.
template<class T>
std::ostream& operator<<(std::ostream&, const std::vector<T>&);

std::string get_program_name(const std::string &);

std::string get_directory_name(const std::string &);

int read_cmdline_parametrs(const int &, const char* const [], std::string &, std::string &, std::string &, std::string &, uint &);
