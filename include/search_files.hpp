#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <queue>
#include <vector>
#include <map>
#include <algorithm>

namespace fs = boost::filesystem;

template <typename Vec>
bool compare_vectors(const Vec& a, const Vec& b)
{
    return a.second.size() > b.second.size();
}

const std::vector<uint> process_file(  const fs::path&,
                    uint&,
                    uint&,
                    std::map<const std::string, std::vector<uint>>&,
                    const std::string&);
      
void worker_thread( std::queue<fs::path>&, 
                    boost::mutex&,
                    uint&,
                    uint&, 
                    std::map<const std::string, std::vector<uint>>&,
                    std::map<const std::string, std::vector<std::string>>&, 
                    const std::string&);

void process_files_with_thread_pool(const std::vector<fs::path>&, 
                                    uint&,
                                    uint&,
                                    std::map<const std::string, std::vector<uint>>&, 
                                    std::map<const std::string, std::vector<std::string>>&, 
                                    const std::string&, 
                                    const uint&);

void delete_old_output_files(const std::string&, const std::string&);

void get_all_files_pathes(std::vector<fs::path>&, std::string);
