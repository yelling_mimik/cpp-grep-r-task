#include "../include/read_parameters.hpp"
#include "../include/search_files.hpp"
#include "../include/output_data.hpp"

// https://gitlab.com/yelling_mimik/cpp-grep-r-task.git

int main(int argc, char *argv[])
{
    std::string input_data, directory_name, log_file_name, result_file_name;
    uint threads_number;

    if (read_cmdline_parametrs(argc, argv, input_data, directory_name, log_file_name, result_file_name, threads_number))
    {
        return 1;
    }

    const std::string pattern = input_data; 
    const auto timer_start = std::chrono::high_resolution_clock::now();

    delete_old_output_files(log_file_name, result_file_name);

    std::vector<fs::path> file_pathes; 
    get_all_files_pathes(file_pathes, directory_name);
    uint searched_files_number = file_pathes.size();

    uint matched_files_number{0};
    uint matched_patterns_number{0};
    std::map<const std::string, std::vector<uint>> results_map;
    std::map<const std::string, std::vector<std::string>> logs_map;

    process_files_with_thread_pool( file_pathes, 
                                    matched_files_number, 
                                    matched_patterns_number, 
                                    results_map, 
                                    logs_map, 
                                    pattern, 
                                    threads_number);

    write_program_data(result_file_name, results_map, &write_data_to_result_file);
    write_program_data(log_file_name, logs_map, &write_data_to_log_file);

    uint program_duration = get_elapsed_time(timer_start, std::chrono::high_resolution_clock::now());

    print_output_to_cmd(searched_files_number, 
                        matched_files_number,
                        matched_patterns_number,
                        result_file_name, 
                        log_file_name, 
                        threads_number, 
                        program_duration);
    return 0;
}