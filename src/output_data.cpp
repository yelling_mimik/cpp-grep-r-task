#include "../include/search_files.hpp"
#include "../include/output_data.hpp"

void get_all_files_pathes(std::vector<fs::path>& file_pathes, std::string root_dir)
{
    fs::recursive_directory_iterator end_iter;
    for (fs::recursive_directory_iterator it(root_dir); it != end_iter; ++it) 
    {
        if (fs::is_regular_file(*it)) 
        {
            file_pathes.push_back(*it);
        }
    }
}

std::string get_line_by_number(const std::string& file_name, const int& line_number)
{
    std::ifstream file(file_name);

    if (file.is_open()) 
    {
        std::string line;
        int current_line{1};

        while (std::getline(file, line)) 
        {
            if (current_line++ == line_number) 
            {
                return line;
            }
        }
    }

    return "";
}

void print_output_to_cmd( const uint& searched_files_number, 
                          const uint& matched_files_number,
                          const uint& matched_patterns_number,
                          const std::string& result_file_name, 
                          const std::string& log_file_name,
                          const uint& threads_number,
                          const uint& program_duration)
{
    std::cout << "Searched files:     " << searched_files_number << "\n";
    std::cout << "Files with pattern: " << matched_files_number << "\n";
    std::cout << "Patterns number:    " << matched_patterns_number << "\n";
    std::cout << "Result file:        " << result_file_name << "\n";
    std::cout << "Log file:           " << log_file_name << "\n";
    std::cout << "Used threads:       " << threads_number << "\n";
    std::cout << "Elapsed time:       " << program_duration << " miliseconds" << "\n";
}

uint get_elapsed_time(const std::chrono::_V2::system_clock::time_point timer_start, const std::chrono::_V2::system_clock::time_point timer_end)
{
    return (std::chrono::duration_cast<std::chrono::milliseconds>(timer_end - timer_start)).count(); 
}
