#include "../include/read_parameters.hpp"

namespace po = boost::program_options;
namespace fs = boost::filesystem;                   

// A helper function to simplify the main part.
template<class T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
    copy(v.begin(), v.end(), std::ostream_iterator<T>(os, " ")); 
    return os;
}

int read_cmdline_parametrs( const int& argc, 
                            const char* const argv[], 
                            std::string& input_data, 
                            std::string& directory_name,
                            std::string& log_file_name, 
                            std::string& result_file_name, 
                            uint& threads_number)
{
    const std::string program_version = "1.0.1";

    const fs::path program_path(argv[0]);
    
    const fs::path log_ext(".log");
    const fs::path txt_ext(".txt");

    const uint default_threads_number = 4;
    const fs::path default_directory_path(program_path.parent_path()); 
    const fs::path default_log_file_path(program_path.filename().replace_extension(log_ext)); 
    const fs::path default_result_file_path(program_path.filename().replace_extension(txt_ext)); 
    const fs::path config_file_relative_path("../config_files/multiple_sources.conf");
    const fs::path default_config_file_to_use(default_directory_path / config_file_relative_path);

    std::string config_file_to_use;
    po::positional_options_description pos_options_desc;
    po::options_description cmdline_options, config_file_options, 
    visible_options("Mandatory positional option: \n\t\t\t\t\tdata to search\n\nAdditional allowed options");

    try
    {
        // Declare a group of options that will be allowed only on command line
        po::options_description generic_options_desc("Generic_options_desc options");
        generic_options_desc.add_options()
            ("version,v", "print version string")
            ("help,h", "produce help message")    
            ("config,c", po::value<std::string>(&config_file_to_use)->default_value(default_config_file_to_use.string()),
                    "name of a configuration file")
            ;
            
        // Declare a group of options that will be allowed both on command line and in config file
        po::options_description config_options_desc("Configuration");
        config_options_desc.add_options()
            ("dir,d", po::value<std::string>(&directory_name)->default_value(default_directory_path.string()),
                "start directory where program needs to look for files (also in subfolders)")
            ("log_file,l", po::value<std::string>(&log_file_name)->default_value(default_log_file_path.string()), 
                "name of log file ")
            ("result_file,r", po::value<std::string>(&result_file_name)->default_value(default_result_file_path.string()), 
                "name of result file")
            ("threads,t", po::value<uint>(&threads_number)->default_value(default_threads_number), 
                "number of threads in the pool")
            ;

        // Hidden options, will be allowed both on command line and in config file, but will not be shown to the user.
        po::options_description hidden_options_desc("Hidden options");
        hidden_options_desc.add_options()
            ("input_data", po::value<std::string>(&input_data), "string to search")
            ;

        cmdline_options.add(generic_options_desc).add(config_options_desc).add(hidden_options_desc);
        config_file_options.add(config_options_desc).add(hidden_options_desc);
        visible_options.add(generic_options_desc).add(config_options_desc);
            
        const char* version_temp_str{"version"}; 
        const char* help_temp_str{"help"}; 
        const char* input_data_temp_str{"input_data"}; 
        const char* dir_temp_str{"dir"}; 
        const char* log_file_temp_str{"log_file"}; 
        const char* result_file_temp_str{"result_file"}; 
        const char* threads_temp_str{"threads"}; 

        pos_options_desc.add(input_data_temp_str, -1);

        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(pos_options_desc).run(), vm);
        po::notify(vm);

        if (vm.count(help_temp_str))
        {
            std::cout << visible_options << "\n";
            return 1;
        }

        if (vm.count(version_temp_str))
        {
            std::cout << "Version:\t" << program_version << "\n";
            return 1;
        }

        std::ifstream ifs(config_file_to_use.c_str());
        if (!ifs)
        {
            std::cout << "Config file:\tnot found\n";
            if (!vm.count(input_data_temp_str))
            {
                std::cout << "Input data not passed in cmd-line nor found in config file" << "\n";
                return 1;
            }
        }
        else
        {
            std::cout << "Config file:\t" << config_file_to_use << "\n";
            po::store(parse_config_file(ifs, config_file_options), vm);
            po::notify(vm);
        }

        if (vm.count(input_data_temp_str))
            std::cout << "Input data:\t" << vm[input_data_temp_str].as<std::string>() << "\n";
        if (vm.count(dir_temp_str))
            std::cout << "Directory:\t" << vm[dir_temp_str].as<std::string>() << "\n";
        if (vm.count(log_file_temp_str))
            std::cout << "Log file:\t" << vm[log_file_temp_str].as<std::string>() << "\n";
        if (vm.count(result_file_temp_str))
            std::cout << "Result file:\t" << vm[result_file_temp_str].as<std::string>() << "\n";
        if (vm.count(threads_temp_str))
            std::cout << "Threads:\t" << vm[threads_temp_str].as<uint>() << "\n";
        
        return 0;
    }
    catch (const po::error& ex)
    {
        std::cerr << ex.what() << "\n";
        std::cout << "\n" << visible_options << "\n";
        return 1;
    }
}
