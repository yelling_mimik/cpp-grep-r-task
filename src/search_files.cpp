#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include "../include/search_files.hpp"

std::vector<uint> process_file(const std::string& file_path_str, const std::string& pattern)
{
    // processed_file processed_file_data{};
    std::ifstream file(file_path_str);
    uint line_number{1};
    std::string line;
    std::vector<uint> matched_pattern_lines_numbers;

    if (!file.is_open()) 
    {
        std::cerr << "Error: could not open file " << file_path_str << "\n";
        return matched_pattern_lines_numbers;
    }

    while (std::getline(file, line)) 
    {
        if (line.find(pattern) != std::string::npos) 
        {
            matched_pattern_lines_numbers.push_back(line_number);
        }
        line_number++;
    }

    return matched_pattern_lines_numbers;
}

void worker_thread( std::queue<fs::path>& task_queue, 
                    boost::mutex& queue_mutex,
                    uint& matched_files_number,
                    uint& matched_patterns_number,
                    std::map<const std::string, std::vector<uint>>& results_map,
                    std::map<const std::string, std::vector<std::string>>& logs_map, 
                    const std::string& pattern)
{
    std::vector<std::string> thread_processed_files_list;
    fs::path file_path;
    uint local_matched_files_number{0};
    uint local_matched_patterns_number{0};

    while (true) 
    {
        // Lock the queue mutex and check if there is a task to do
        queue_mutex.lock();
        if (!task_queue.empty()) 
        {
            // Get the next task from the queue
            file_path = task_queue.front();
            task_queue.pop();
            queue_mutex.unlock();
        } 
        else 
        {
            // If there are no tasks left break loop and go to writing down thread logs
            queue_mutex.unlock();
            break;
        }

        const std::string file_path_str = file_path.string();

        thread_processed_files_list.push_back(file_path_str);
        const std::vector<uint> matched_pattern_lines_numbers(process_file(file_path_str, pattern));

        if (!matched_pattern_lines_numbers.empty())
        {
            local_matched_files_number++;
            local_matched_patterns_number+=matched_pattern_lines_numbers.size();

            // Lock guard the queue mutex before modifing variable logs_map, that is common to all worker threads
            boost::lock_guard<boost::mutex> lock(queue_mutex);  
            results_map.insert(std::make_pair(file_path_str, matched_pattern_lines_numbers));
        }
    }

    const std::string threadId = boost::lexical_cast<std::string>(boost::this_thread::get_id());
    const std::string thread_processed_files_num = std::to_string(thread_processed_files_list.size());
    const std::string threadId_and_processed_files_num = threadId + " : " + thread_processed_files_num;
    
    // Lock guard the queue mutex before modifing variables common to all worker threads
    boost::lock_guard<boost::mutex> lock(queue_mutex);
    matched_files_number+=local_matched_files_number;
    matched_patterns_number+=local_matched_patterns_number;
    logs_map.insert(std::make_pair(threadId_and_processed_files_num, thread_processed_files_list));
}

void process_files_with_thread_pool(const std::vector<fs::path>& file_pathes, 
                                    uint& matched_files_number,
                                    uint& matched_patterns_number,
                                    std::map<const std::string, std::vector<uint>>& results_map, 
                                    std::map<const std::string, std::vector<std::string>>& logs_map, 
                                    const std::string& pattern, 
                                    const uint& threads_number)
{
    // Create a task queue with the files to process
    std::queue<fs::path> task_queue;
    for (const auto& file_path : file_pathes) 
    {
        task_queue.push(file_path);
    }

    // Create a mutex to protect the task queue
    boost::mutex queue_mutex;

    // Create a thread pool and timer_start the worker threads
    boost::thread_group thread_pool;
    for (size_t i = 0; i < threads_number; ++i) 
    {
        thread_pool.create_thread(boost::bind(worker_thread, 
                                              boost::ref(task_queue), 
                                              boost::ref(queue_mutex),
                                              boost::ref(matched_files_number),
                                              boost::ref(matched_patterns_number), 
                                              boost::ref(results_map),
                                              boost::ref(logs_map),
                                              boost::ref(pattern)));
    }

    // Wait for all the worker threads to finish
    thread_pool.join_all();
}

void delete_old_output_files(const std::string& result_file_name, const std::string& log_file_name)
{
    fs::path result_file_path(result_file_name);
    fs::path log_file_path(log_file_name);

    if (fs::exists(result_file_path))
    {
        fs::remove(result_file_path);
    }

    if (fs::exists(log_file_path))
    {
        fs::remove(log_file_path);
    }
}
